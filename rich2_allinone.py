# from when to when would you like the trendplot, format is YYYYMMDD
# !!! For RICH1 the period between 20160524 to 20160824 cannot be divided up, it either minDate or maxDate lie within that range the entire range will be added
minDate = 20170605 # FIRST MAG UP FILL OF 2017
#maxDate = 20170713 # BEFORE FIRST MAG DOWN FILL OF 2017
maxDate = 20170829 # INFINITY
# fill this if you know which alignments you want to use by specifying the foldername within /group/online/AligWork/MirrorAlignments/RichN/, e.g. '20160229_002009'
alignmentList = []
# fill this if there are certain alignments during you time-period you don't want to go into the plot by specifying the foldername within /group/online/AligWork/MirrorAlignments/RichN/, e.g. '20160229_002009'
exceptionList = ['20170606_102357',
'20170610_211541',
'20170611_170020',
'20170611_175943',
'20170611_190207',
'20170611_201742',
'20170613_083436',
'20170621_180226',
'20170627_105541',
'20170705_185414',
'20170706_100355',
'20170706_162516',
'20170708_140804',
#'20170714_100603',
'20170715_162726',
'20170716_165032',
'20170810_115802',
'20170810_152825',
'20170810_154107',
'20170810_155418',
'20170814_153345',
'20170814_154437'
]


# set up some stuff
whichRich = 2
if(whichRich == 1):
    maxpri = 3
    maxsec = 15
if(whichRich == 2):
    maxpri = 55
    maxsec = 39


# Functions declared here

def AlignmentSelector(i, alignments, name):
    if name == 'Old':
        alignment_temp = alignments[0]
        return alignment_temp, 1, 0
    if name == '20170717':
                alignment_temp = alignments[0]
		if i < 19:
                        alignment_temp = alignments[0]
                        return alignment_temp, 1, 0
                if i >= 19 and i < 20:
                        alignment_temp = alignments[19]
                        return alignment_temp, 1, 19
		if i >= 20 and i < 23:
                        alignment_temp = alignments[20]
                        return alignment_temp, 1, 20
                if i >= 23 and i < 48:
                        alignment_temp = alignments[23]
                        return alignment_temp, 1, 23
                if i >= 48:
                        alignment_temp = alignments[48]
                        return alignment_temp, 1, 47

    if name == 'Custom':
                alignment_temp = alignments[0]
                if i < 5:
                        alignment_temp = alignments[0]
                        return alignment_temp, 1, 0
                if i >= 5 and i < 46:
                        alignment_temp = alignments[5]
                        return alignment_temp, 1, 5
#		if i >= 9 and i < 46:
#			alignment_temp=alignments[9]
#			return alignment_temp, 1, 9
                if i >=46 and i < 72:
                        alignment_temp = alignments[46]
                        return alignment_temp, 1, 46
                if i >= 72 and i < 79:
                        alignment_temp = alignments[72]
                        return alignment_temp, 1, 72
                if i >= 79:
                        alignment_temp = alignments[79]
                        return alignment_temp, 1, 79

    if name == 'EachFlip':
		alignment_temp = alignments[0]
		if i < 5:
			alignment_temp = alignments[0]
			return alignment_temp, 1
		if i >= 5 and i < 46:
			alignment_temp = alignments[5]
			return alignment_temp, 1
		if i >=46 and i < 72:
			alignment_temp = alignments[46]
			return alignment_temp, 1
		if i >= 72 and i < 79:
			alignment_temp = alignments[72]
			return alignment_temp, 1
		if i >= 79:
			alignment_temp = alignments[79]
			return alignment_temp, 1

    elif name == 'MagUp':
	    #       For mag up only of the original alignment
	    alignment_temp = alignments[4]
	    if i < 5:
		alignment_temp = alignments[0]
		return alignment_temp, 0
	    if i >= 5 and i < 46:
		alignment_temp = alignments[5]
		return alignment_temp, 1
	    if i >=46 and i < 72:
		alignment_temp = alignments[0]
	        return alignment_temp, 0
	    if i >= 72 and i < 79:
			alignment_temp = alignments[5]
			return alignment_temp, 1
	    if i >= 79:
		alignment_temp = alignments[0]
		return alignment_temp, 0

    elif name == 'MagDown':
		#       For mag up only of the original alignment
		alignment_temp = alignments[0]
		if i < 5:
			alignment_temp = alignments[0]
			return alignment_temp, 1
		if i >= 5 and i < 46:
			alignment_temp = alignments[0]
			return alignment_temp, 0
		if i >=46 and i < 72:
			alignment_temp = alignments[0]
			return alignment_temp, 1
		if i >= 72 and i < 79:
			alignment_temp = alignments[0]
			return alignment_temp, 0
		if i >= 79:
			alignment_temp = alignments[0]
			return alignment_temp, 1

def limit_check(tilts):
    pY = 0.065
    pZ = 0.065
    sY = 0.055
    sZ = 0.035
    realign = 0

    # Primary Y
    for i in xrange(56):
	if abs(tilts[0][i]) > pY:
	    print tilts[0][i]
	    realign = 1
	if abs(tilts[1][i]) > pZ:
	    print tilts[1][i]
	    realign = 1
    # Secondary
    for i in xrange(40):
	if abs(tilts[2][i]) > sY:
	    print tilts[2][i]
	    realign = 1
	if abs(tilts[3][i]) > sZ:
	    print tilts[3][i]
	    realign = 1
    return realign


#########################
#
from fileFinder import fileFinder
print "exceptionList: ", exceptionList
findFiles = fileFinder(whichRich, minDate, maxDate, alignmentList, exceptionList)
files = findFiles.findFiles()
alignments = findFiles.alignments

from tiltObj import tiltObj
tiltmaker = tiltObj(whichRich)

from ROOT import TCanvas, TGraph, gStyle, TMultiGraph, TLine

from LHCbStyle import LHCbStyle
lhcbStyle = LHCbStyle()
lhcbStyle.SetTitleOffset(0.8,"Y")
lhcbStyle.SetTitleFont(132, "Y")

graphs_priY = {}
graphs_priZ = {}
graphs_secY = {}
graphs_secZ = {}
for j in range(0, maxpri + 1):
    graphs_priY[j] = TGraph(len(alignments))
    graphs_priZ[j] = TGraph(len(alignments))
for j in range(0, maxsec + 1):
    graphs_secY[j] = TGraph(len(alignments))
    graphs_secZ[j] = TGraph(len(alignments))

counter = 0
ReAlign = 0
NEW_alignment_number = 0
print "alignments: ", alignments
for i in range(0, len(alignments)):
    print i, alignments[i]
    alignment = alignments[i]

    # alignment_temp, cont_flag = AlignmentSelector(i, alignments, 'Old')
    alignment_temp, cont_flag, triggered_tag = AlignmentSelector(i, alignments, '20170717')
    if cont_flag == 0:
        continue

    if i == triggered_tag:
        print('MIRROR FLIP HERE -------------------------------> ')
    if NEW_alignment_number > triggered_tag:
        alignment_temp = alignments[NEW_alignment_number]
        print('Aligning with (from limits):',alignment_temp)

    print('Aligning with:',alignment_temp)
    tilts = tiltmaker.getChange([files[alignment_temp][1],files[alignment][1]])
#    ReAlign = limit_check(tilts)
    if ReAlign == 1:
        NEW_alignment_number = i
        ReAlign = 0
        if NEW_alignment_number > triggered_tag:
                alignment_temp = alignments[NEW_alignment_number]
                print('Aligning with (update here):',alignment_temp)
                tilts = tiltmaker.getChange([files[alignment_temp][1],files[alignment][1]])
    for j in range(0, maxpri + 1):
        graphs_priY[j].SetPoint(i, i, tilts[0][j])
        graphs_priZ[j].SetPoint(i, i, tilts[1][j])
    for j in range(0, maxsec + 1):
        graphs_secY[j].SetPoint(i, i, tilts[2][j])
        graphs_secZ[j].SetPoint(i, i, tilts[3][j])


multisPriY = TMultiGraph()
multisPriZ = TMultiGraph()
multisSecY = TMultiGraph()
multisSecZ = TMultiGraph()
markers = [20, 21, 22, 23]
for j in range(0, maxpri + 1):
    graphs_priY[j].SetMarkerStyle(markers[j%4])
    graphs_priY[j].SetMarkerSize(1)
    graphs_priY[j].SetMarkerColor(45+j)
    graphs_priY[j].SetLineColor(45+j)

    graphs_priZ[j].SetMarkerStyle(markers[j%4])
    graphs_priZ[j].SetMarkerSize(1)
    graphs_priZ[j].SetMarkerColor(45+j)
    graphs_priZ[j].SetLineColor(45+j)

    multisPriY.Add(graphs_priY[j])
    multisPriZ.Add(graphs_priZ[j])

for j in range(0, maxsec + 1):
    graphs_secY[j].SetMarkerStyle(markers[j%4])
    graphs_secY[j].SetMarkerSize(1)
    graphs_secY[j].SetMarkerColor(61+j)
    graphs_secY[j].SetLineColor(61+j)

    graphs_secZ[j].SetMarkerStyle(markers[j%4])
    graphs_secZ[j].SetMarkerSize(1)
    graphs_secZ[j].SetMarkerColor(61+j)
    graphs_secZ[j].SetLineColor(61+j)
    multisSecY.Add(graphs_secY[j])
    multisSecZ.Add(graphs_secZ[j])

middle = TLine(-0.65, 0, len(alignments), 0)
pri_upper = TLine (-0.65, 0.1, 14.5, 0.1)
pri_lower = TLine (-0.65, -0.1, 14.5, -0.1)
pri_upper.SetLineStyle(7)
pri_lower.SetLineStyle(7)

# 2017 thresholds
pY_17 = 0.03
pZ_17 = 0.03
sY_17 = 0.05
sZ_17 = 0.06

multisPriY.SetMinimum(-4*pY_17)
multisPriY.SetMaximum(4*pY_17)
multisPriZ.SetMinimum(-4*pZ_17)
multisPriZ.SetMaximum(4*pZ_17)
multisSecY.SetMinimum(-4*sY_17)
multisSecY.SetMaximum(4*sY_17)
multisSecZ.SetMinimum(-4*sZ_17)
multisSecZ.SetMaximum(4*sZ_17)

# 2017 Threshold Lines
pY_17_line1 = TLine(-1, pY_17 , len(alignments), pY_17)
pY_17_line2 = TLine(-1, -pY_17 , len(alignments), -pY_17)
pY_17_line1.SetLineStyle(7)
pY_17_line2.SetLineStyle(7)
pZ_17_line1 = TLine(-1, pZ_17 , len(alignments), pZ_17)
pZ_17_line2 = TLine(-1, -pZ_17 , len(alignments), -pZ_17)
pZ_17_line1.SetLineStyle(7)
pZ_17_line2.SetLineStyle(7)
sY_17_line1 = TLine(-1, sY_17 , len(alignments), sY_17)
sY_17_line2 = TLine(-1, -sY_17 , len(alignments), -sY_17)
sY_17_line1.SetLineStyle(7)
sY_17_line2.SetLineStyle(7)
sZ_17_line1 = TLine(-1, sZ_17 , len(alignments), sZ_17)
sZ_17_line2 = TLine(-1, -sZ_17 , len(alignments), -sZ_17)
sZ_17_line1.SetLineStyle(7)
sZ_17_line2.SetLineStyle(7)

#   2017 Magnet polarity switches
flip1pY_17 = TLine(18.5, -4*pY_17, 18.5, 4*pY_17) # large vertical limits)
flip1pY_17.SetLineStyle(7)
flip1pZ_17 = TLine(18.5, -4*pZ_17, 18.5, 4*pZ_17) # large vertical limits)
flip1pZ_17.SetLineStyle(7)
flip1sY_17 = TLine(18.5, -4*sY_17, 18.5, 4*sY_17) # large vertical limits)
flip1sY_17.SetLineStyle(7)
flip1sZ_17 = TLine(18.5, -4*sZ_17, 18.5, 4*sZ_17) # large vertical limits)
flip1sZ_17.SetLineStyle(7)

flip2pY_17 = TLine(47.5, -4*pY_17, 47.5, 4*pY_17) # large vertical limits)
flip2pY_17.SetLineStyle(7)
flip2pZ_17 = TLine(47.5, -4*pZ_17, 47.5, 4*pZ_17) # large vertical limits)
flip2pZ_17.SetLineStyle(7)
flip2sY_17 = TLine(47.5, -4*sY_17, 47.5, 4*sY_17) # large vertical limits)
flip2sY_17.SetLineStyle(7)
flip2sZ_17 = TLine(47.5, -4*sZ_17, 47.5, 4*sZ_17) # large vertical limits)
flip2sZ_17.SetLineStyle(7)

#   Magnet polarity switches
flip1 = TLine(4.5, -0.25, 4.5, 0.25)
flip1.SetLineStyle(7)
flip2 = TLine(45.5, -0.25, 45.5, 0.25)
flip2.SetLineStyle(7)
flip3 = TLine(71.5, -0.25, 71.5, 0.25)
flip3.SetLineStyle(7)
flip4 = TLine(78.5, -0.25, 78.5, 0.25)
flip4.SetLineStyle(7)

#   Mag polarity switches for Seconadry
flip1s = TLine(4.5, -0.25, 4.5, 0.25)
flip1s.SetLineStyle(7)
flip2s = TLine(45.5, -0.25, 45.5, 0.25)
flip2s.SetLineStyle(7)
flip3s = TLine(71.5, -0.25, 71.5, 0.25)
flip3s.SetLineStyle(7)
flip4s = TLine(78.5, -0.25, 78.5, 0.25)
flip4s.SetLineStyle(7)

from ROOT import TPaveText
txt = TPaveText(.2, .7, 0.45, 0.9, "NDC")
txt.AddText('#scale[1.5]{LHCb RICH 2}')
txt.SetFillStyle(1001)
txt.SetBorderSize(0)
txt.SetFillColor(0)

prim = TPaveText(0.68, 0.9, .9, 0.80, 'NDC')
prim.AddText( 'Primary Mirrors')
prim.SetFillColor(0)
seco = TPaveText(0.68, 0.9, .9, 0.80, 'NDC')
seco.AddText( 'Secondary Mirrors')
seco.SetFillColor(0)
from ROOT import TPaveText
date = TPaveText(.65,.2,0.9,0.25, 'NDC')
date.AddText( '2017/06/05 - 2017/08/29')
date.SetFillColor(0)


leg2a = TPaveText(0.68, 0.7, .9, 0.8, 'NDC')
leg2a.AddText( 'Markers represent')
leg2a.AddText( 'the 56 individual mirrors')
leg2a.SetFillColor(0)

leg2b = TPaveText(0.68, 0.7, .9, 0.8, 'NDC')
leg2b.AddText( 'Markers represent')
leg2b.AddText( 'the 40 individual mirrors')
leg2b.SetFillColor(0)

cPlot = TCanvas('hasi', 'masi')
cPlot.SaveAs("plots/Rich2_allin1_20170717.pdf[")
multisPriY.Draw('AP')
multisPriY.GetXaxis().SetTitle("Alignment number [a.u.]")
multisPriY.GetYaxis().SetTitle("#DeltaY Variation [mrad]")
txt.InsertText('#scale[1.3]{Preliminary}')
flip1pY_17.Draw('same')
flip2pY_17.Draw('same')
pY_17_line1.Draw('same')
pY_17_line2.Draw('same')
txt.Draw('same')
date.Draw('same')
# leg2a.InsertText('the 56 individual mirrors.')
leg2a.Draw('same')
middle.Draw('same')
#flip1.Draw('same')
#flip2.Draw('same')
#flip3.Draw('same')
#flip4.Draw('same')
prim.Draw('same')
#pri_upper.Draw('same')
#pri_lower.Draw('same')
cPlot.SaveAs("plots/Rich2_allin1_20170717.pdf")

multisPriZ.Draw('AP')
multisPriZ.GetXaxis().SetTitle("Alignment number [a.u.]")
multisPriZ.GetYaxis().SetTitle("#DeltaZ Variation [mrad]")
flip1pZ_17.Draw('same')
flip2pZ_17.Draw('same')
pZ_17_line1.Draw('same')
pZ_17_line2.Draw('same')
txt.Draw('same')
date.Draw('same')
# leg2a.InsertText('the 56 individual mirrors.')
leg2a.Draw('same')
#pri_upper.Draw('same')
#pri_lower.Draw('same')
middle.Draw('same')
#flip1.Draw('same')
#flip2.Draw('same')
#flip3.Draw('same')
#flip4.Draw('same')
prim.Draw('same')
cPlot.SaveAs("plots/Rich2_allin1_20170717.pdf")

multisSecY.Draw('AP')
multisSecY.GetXaxis().SetTitle("Alignment number [a.u.]")
multisSecY.GetYaxis().SetTitle("#DeltaY Variation [mrad]")
flip1sY_17.Draw('same')
flip2sY_17.Draw('same')
sY_17_line1.Draw('same')
sY_17_line2.Draw('same')
txt.Draw('same')
leg2b.Draw('same')
#leg2b.Draw('same')
date.Draw('same')
middle.Draw('same')
#flip1s.Draw('same')
#flip2s.Draw('same')
#flip3s.Draw('same')
#flip4s.Draw('same')
seco.Draw('same')
#pri_upper.Draw('same')
#pri_lower.Draw('same')
cPlot.SaveAs("plots/Rich2_allin1_20170717.pdf")

multisSecZ.Draw('AP')
multisSecZ.GetXaxis().SetTitle("Alignment number [a.u.]")
multisSecZ.GetYaxis().SetTitle("#DeltaZ Variation [mrad]")
flip1sZ_17.Draw('same')
flip2sZ_17.Draw('same')
sZ_17_line1.Draw('same')
sZ_17_line2.Draw('same')
txt.Draw('same')
leg2b.Draw('same')
#leg2b.Draw('same')
date.Draw('same')
middle.Draw('same')
#flip1s.Draw('same')
#flip2s.Draw('same')
#flip3s.Draw('same')
#flip4s.Draw('same')
seco.Draw('same')
#pri_upper.Draw('same')
#pri_lower.Draw('same')
cPlot.SaveAs("plots/Rich2_allin1_20170717.pdf")

cPlot.SaveAs("plots/Rich2_allin1_20170717.pdf]")

#  LocalWords:  pri
