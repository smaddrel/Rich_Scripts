# from when to when would you like the trendplot, format is YYYYMMDD
# !!! For RICH1 the period between 20160524 to 20160824 cannot be divided up, it either minDate or maxDate lie within that range the entire range will be added
minDate = 20170602 # FIRST MAG UP FILL OF 2017
#maxDate = 20170713 # BEFORE FIRST MAG DOWN FILL OF 2017
maxDate = 20170829 # INFINITY
# fill this if you know which alignments you want to use by specifying the foldername within /group/online/AligWork/MirrorAlignments/RichN/, e.g. '20160229_002009'
alignmentList = []
# fill this if there are certain alignments during you time-period you don't want to go into the plot by specifying the foldername within /group/online/AligWork/MirrorAlignments/RichN/, e.g. '20160229_002009'
exceptionList = ['20170606_150337',
'20170611_143511',
'20170611_192654',
'20170612_004007',
'20170621_114735',
'20170621_142043',
'20170621_151343',
'20170621_153621',
'20170621_183028',
'20170705_184436',
'20170706_093538',
'20170706_160847',
'20170708_130756',
'20170708_145416',
# '20170714_095506',
'20170715_161843',
'20170716_164021',
'20170717_173507',
'20170717_174549',
'20170717_175536',
'20170717_210458',
'20170717_212834',
'20170808_121449',
'20170808_150718',
'20170808_151700',
'20170808_152604',
'20170808_153952',
'20170808_155511',
'20170810_083727',
'20170810_085136',
'20170810_090208',
'20170810_091600',
'20170810_092531',
'20170810_110324',
'20170810_113408',
'20170814_144647',
'20170814_150050',
'20170814_152611'
]

# set up some stuff
whichRich = 1
if(whichRich == 1):
    maxpri = 3
    maxsec = 15
if(whichRich == 2):
    maxpri = 55
    maxsec = 39
# Functions declared here

def AlignmentSelector(i, alignments, name):
    if name == 'Old':
	alignment_temp = alignments[0]
	return alignment_temp, 1, 0
    if name == '20170717':
                alignment_temp = alignments[0]
		if i < 19:
			alignment_temp = alignments[0]
			return alignment_temp, 1, 0
		if i >= 19 and i < 24:
			alignment_temp = alignments[19]
			return alignment_temp, 1, 19
		if i >= 24 and i < 49:
			alignment_temp = alignments[24]
			return alignment_temp, 1, 24
		if i >= 49:
			alignment_temp = alignments[49]
                        return alignment_temp, 1, 49

    if name == 'Custom':
                alignment_temp = alignments[0]
		if i < 6:
			alignment_temp = alignments[0]
			return alignment_temp, 1, 0
		if i >= 6 and i < 11:
			alignment_temp = alignments[6]
			return alignment_temp, 1, 6
		if i >=11 and i < 20:
			alignment_temp = alignments[11]
			return alignment_temp, 1, 11
		if i >= 20 and i < 27:
			alignment_temp = alignments[20]
			return alignment_temp, 1, 20
		if i >= 27:
			alignment_temp = alignments[27]
			return alignment_temp, 1, 27
    if name == 'EachFlip':
	#This is for the align .wrt. the first new alignment after each polarity flip.
	#print files[alignment]
	alignment_temp = alignments[0]
	if i < 6:
                alignment_temp = alignments[0]
                return alignment_temp, 1
        if i >= 6 and i < 11:
                alignment_temp = alignments[6]
                return alignment_temp, 1
        if i >=11 and i < 20:
                alignment_temp = alignments[11]
                return alignment_temp, 1
        if i >= 20 and i < 27:
                alignment_temp = alignments[20]
                return alignment_temp, 1
        if i >= 27:
                alignment_temp = alignments[27]
                return alignment_temp, 1

    elif name == 'MagUp':
	#       For mag up only of the original alignment
	alignment_temp = alignments[6]
	if i < 6:
		alignment_temp = alignments[0]
	    	return alignment_temp, 0
	if i >= 6 and i < 11:
                alignment_temp = alignments[6]
		return alignment_temp, 1
	if i >=11 and i < 20:
		alignment_temp = alignments[11]
	    	return alignment_temp, 0
	if i >= 20 and i < 27:
                alignment_temp = alignments[6]
                return alignment_temp, 1
	if i >= 27:
		alignment_temp = alignments[27]
		return alignment_temp, 0

    elif name == 'MagDown':
	#       For mag up only of the original alignment
	alignment_temp = alignments[0]
	if i < 6:
                alignment_temp = alignments[0]
                return alignment_temp, 1
        if i >= 6 and i < 11:
                alignment_temp = alignments[6]
                return alignment_temp, 0
        if i >=11 and i < 20:
                alignment_temp = alignments[0]
                return alignment_temp, 1
        if i >= 20 and i < 27:
                alignment_temp = alignments[20]
                return alignment_temp, 0
        if i >= 27:
                alignment_temp = alignments[0]
                return alignment_temp, 1

def limit_check(tilts):
    pY = 0.05
    pZ = 0.06
    sY = 0.25
    sZ = 0.30
    realign = 0

    # Primary Y
    for i in xrange(4):
        if abs(tilts[0][i]) > pY:
            realign = 1
	    print('pY at :', i, 'value: ', tilts[0][i])
        if abs(tilts[1][i]) > pZ:
            realign = 1
	    print('pZ at :', i, 'value: ', tilts[1][i])
    # Secondary
    for i in xrange(15):
        if abs(tilts[2][i]) > sY:
            realign = 1
	    print('sY at :', i, 'value: ', tilts[2][i])
        if abs(tilts[3][i]) > sZ:
            realign = 1
	    print('sZ at :', i, 'value: ', tilts[3][i])
    return realign

#########################
from fileFinder import fileFinder
print "exceptionList: ", exceptionList
findFiles = fileFinder(whichRich, minDate, maxDate, alignmentList, exceptionList)
files = findFiles.findFiles()
alignments = findFiles.alignments
print 'Check the alignment numbers:'
for i, a in zip(xrange(len(alignments)), alignments):
    print i, a
from tiltObj import tiltObj
tiltmaker = tiltObj(whichRich)

from ROOT import TCanvas, TGraph, gStyle, TMultiGraph, TLine

from LHCbStyle import LHCbStyle
lhcbStyle = LHCbStyle()
lhcbStyle.SetTitleOffset(0.8,"Y")
lhcbStyle.SetTitleFont(132, "Y")

graphs_priY = {}
graphs_priZ = {}
graphs_secY = {}
graphs_secZ = {}
for j in range(0, maxpri + 1):
    graphs_priY[j] = TGraph(len(alignments))
    graphs_priZ[j] = TGraph(len(alignments))
for j in range(0, maxsec + 1):
    graphs_secY[j] = TGraph(len(alignments))
    graphs_secZ[j] = TGraph(len(alignments))
#print files
counter = 0
ReAlign = 0
NEW_alignment_number = 0
print "alignments: ", alignments
for i in range(0, len(alignments)):
    alignment = alignments[i]
    print i, alignment

    # alignment_temp, cont_flag = AlignmentSelector(i, alignments, 'Old')
    alignment_temp, cont_flag, triggered_tag = AlignmentSelector(i, alignments, '20170717')
    if cont_flag == 0:
        continue

    if i == triggered_tag:
	print('MIRROR FLIP HERE -------------------------------> ')
    if NEW_alignment_number > triggered_tag:
	alignment_temp = alignments[NEW_alignment_number]
	print('Aligning with (from limits):',alignment_temp)

    print('Aligning with:',alignment_temp)
    tiltmaker.setChange([files[alignment_temp][1],files[alignment][1]])
    tilts = tiltmaker.getGraphs()
#    ReAlign = limit_check(tilts)
    if ReAlign == 1:
	NEW_alignment_number = i
	ReAlign = 0
        if NEW_alignment_number > triggered_tag:
		alignment_temp = alignments[NEW_alignment_number]
		print('Aligning with (update here):',alignment_temp)
		tiltmaker.setChange([files[alignment_temp][1],files[alignment][1]])
        tilts = tiltmaker.getGraphs()



    for j in range(0, maxpri + 1):
        graphs_priY[j].SetPoint(i, i, tilts[0][j])
        graphs_priZ[j].SetPoint(i, i, tilts[1][j])
    for j in range(0, maxsec + 1):
        graphs_secY[j].SetPoint(i, i, tilts[2][j])
        graphs_secZ[j].SetPoint(i, i, tilts[3][j])
        #if abs(tilts[3][j]) > 0.2:
        #    print j, ':', tilts[3][j]
multisPriY = TMultiGraph()
multisPriZ = TMultiGraph()
multisSecY = TMultiGraph()
multisSecZ = TMultiGraph()
markers = [20, 21, 22, 23]
for j in range(0, maxpri + 1):
    graphs_priY[j].SetMarkerStyle(markers[j%4])
    graphs_priY[j].SetMarkerSize(1)
    graphs_priY[j].SetMarkerColor(2+2*j)
    graphs_priY[j].SetLineColor(2+2*j)

    graphs_priZ[j].SetMarkerStyle(markers[j%4])
    graphs_priZ[j].SetMarkerSize(1)
    graphs_priZ[j].SetMarkerColor(2+2*j)
    graphs_priZ[j].SetLineColor(2+2*j)

multisPriY.Add(graphs_priY[1])
multisPriY.Add(graphs_priY[0])
multisPriY.Add(graphs_priY[2])
multisPriY.Add(graphs_priY[3])

multisPriZ.Add(graphs_priZ[1])
multisPriZ.Add(graphs_priZ[0])
multisPriZ.Add(graphs_priZ[2])
multisPriZ.Add(graphs_priZ[3])

for j in range(0, maxsec + 1):
    graphs_secY[j].SetMarkerStyle(markers[j%4])
    graphs_secY[j].SetMarkerSize(1)
    graphs_secY[j].SetMarkerColor(55+3*j)
    graphs_secY[j].SetLineColor(55+3*j)

    graphs_secZ[j].SetMarkerStyle(markers[j%4])
    graphs_secZ[j].SetMarkerSize(1)
    graphs_secZ[j].SetMarkerColor(55+3*j)
    graphs_secZ[j].SetLineColor(55+3*j)
    multisSecY.Add(graphs_secY[j])
    multisSecZ.Add(graphs_secZ[j])

from ROOT import TPaveText
txt = TPaveText(.2, .7, 0.45, 0.9, "NDC")
txt.AddText('#scale[1.5]{LHCb RICH 1}')
txt.SetFillStyle(1001)
txt.SetBorderSize(0)
txt.SetFillColor(0)

prim = TPaveText(0.68, 0.9, .9, 0.80, 'NDC')
prim.AddText( 'Primary Mirrors')
prim.SetFillColor(0)
seco = TPaveText(0.68, 0.9, .9, 0.80, 'NDC')
seco.AddText( 'Secondary Mirrors')
seco.SetFillColor(0)
from ROOT import TPaveText
date = TPaveText(.65,.2,0.9,0.25, 'NDC')
date.AddText( '2017/06/06 - 2017/08/29')
date.SetFillColor(0)


leg2a = TPaveText(0.68, 0.7, .9, 0.8, 'NDC')
leg2a.AddText( 'Markers represent')
leg2a.SetFillColor(0)
#leg2a.InsertText('individual mirrors.')

from ROOT import TLegend
leg = TLegend(0.68, 0.62, .9, .82)
leg.AddEntry(graphs_priY[0], "primary mirror 0", 'p')
leg.AddEntry(graphs_priY[1], "primary mirror 1", 'p')
leg.AddEntry(graphs_priY[2], "primary mirror 2", 'p')
leg.AddEntry(graphs_priY[3], "primary mirror 3", 'p')
leg.SetFillColor(0)
leg.SetTextSize(0.04)
leg.SetTextFont(132)

pri_upper = TLine (-0.5, 0.1, 14.5, 0.1)
pri_lower = TLine (-0.5, -0.1, 14.5, -0.1)
pri_upper.SetLineStyle(7)
pri_lower.SetLineStyle(7)
sec_upper =TLine (-0.5, 0.2, 14.5, 0.2)
sec_lower = TLine (-0.5, -0.2, 14.5, -0.2)
sec_upper.SetLineStyle(7)
sec_lower.SetLineStyle(7)
middle = TLine(-0.5, 0, len(alignments), 0)

# 2017 thresholds
pY_17 = 0.03
pZ_17 = 0.03
sY_17 = 0.46
sZ_17 = 0.37

multisPriY.SetMinimum(-4*pY_17)
multisPriY.SetMaximum(4*pY_17)
multisPriZ.SetMinimum(-4*pZ_17)
multisPriZ.SetMaximum(4*pZ_17)
multisSecY.SetMinimum(-4*sY_17)
multisSecY.SetMaximum(4*sY_17)
multisSecZ.SetMinimum(-4*sZ_17)
multisSecZ.SetMaximum(4*sZ_17)

# 2017 Threshold Lines
pY_17_line1 = TLine(-1, pY_17 , len(alignments), pY_17)
pY_17_line2 = TLine(-1, -pY_17 , len(alignments), -pY_17)
pY_17_line1.SetLineStyle(7)
pY_17_line2.SetLineStyle(7)
pZ_17_line1 = TLine(-1, pZ_17 , len(alignments), pZ_17)
pZ_17_line2 = TLine(-1, -pZ_17 , len(alignments), -pZ_17)
pZ_17_line1.SetLineStyle(7)
pZ_17_line2.SetLineStyle(7)
sY_17_line1 = TLine(-1, sY_17 , len(alignments), sY_17)
sY_17_line2 = TLine(-1, -sY_17 , len(alignments), -sY_17)
sY_17_line1.SetLineStyle(7)
sY_17_line2.SetLineStyle(7)
sZ_17_line1 = TLine(-1, sZ_17 , len(alignments), sZ_17)
sZ_17_line2 = TLine(-1, -sZ_17 , len(alignments), -sZ_17)
sZ_17_line1.SetLineStyle(7)
sZ_17_line2.SetLineStyle(7)

#   2017 Magnet polarity switches
flip1pY_17 = TLine(18.5, -4*pY_17, 18.5, 4*pY_17) # large vertical limits)
flip1pY_17.SetLineStyle(7)
flip1pZ_17 = TLine(18.5, -4*pZ_17, 18.5, 4*pZ_17) # large vertical limits)
flip1pZ_17.SetLineStyle(7)
flip1sY_17 = TLine(18.5, -4*sY_17, 18.5, 4*sY_17) # large vertical limits)
flip1sY_17.SetLineStyle(7)
flip1sZ_17 = TLine(18.5, -4*sZ_17, 18.5, 4*sZ_17) # large vertical limits)
flip1sZ_17.SetLineStyle(7)

flip2pY_17 = TLine(48.5, -4*pY_17, 48.5, 4*pY_17) # large vertical limits)
flip2pY_17.SetLineStyle(7)
flip2pZ_17 = TLine(48.5, -4*pZ_17, 48.5, 4*pZ_17) # large vertical limits)
flip2pZ_17.SetLineStyle(7)
flip2sY_17 = TLine(48.5, -4*sY_17, 48.5, 4*sY_17) # large vertical limits)
flip2sY_17.SetLineStyle(7)
flip2sZ_17 = TLine(48.5, -4*sZ_17, 48.5, 4*sZ_17) # large vertical limits)
flip2sZ_17.SetLineStyle(7)

#	Magnet polarity switches
flip1 = TLine(5.5, -0.3, 5.5, 0.3)
flip1.SetLineStyle(7)
flip2 = TLine(10.5, -0.3, 10.5, 0.3)
flip2.SetLineStyle(7)
flip3 = TLine(19.5, -0.3, 19.5, 0.3)
flip3.SetLineStyle(7)
flip4 = TLine(26.5, -0.3, 26.5, 0.3)
flip4.SetLineStyle(7)

#	Mag polarity switches for Seconadry
flip1s = TLine(5.5, -0.65, 5.5, 0.65)
flip1s.SetLineStyle(7)
flip2s = TLine(10.5, -0.65, 10.5, 0.65)
flip2s.SetLineStyle(7)
flip3s = TLine(19.5, -0.65, 19.5, 0.65)
flip3s.SetLineStyle(7)
flip4s = TLine(26.5, -0.65, 26.5, 0.65)
flip4s.SetLineStyle(7)


cPlot = TCanvas('hasi', 'masi')
cPlot.SaveAs("plots/Rich1_allin1_20170830.pdf[")
multisPriY.Draw('AP')
multisPriY.GetXaxis().SetTitle("Alignment number [a.u.]")
multisPriY.GetYaxis().SetTitle("#DeltaY Variation [mrad]")
flip1pY_17.Draw('same')
flip2pY_17.Draw('same')
pY_17_line1.Draw('same')
pY_17_line2.Draw('same')
txt.InsertText('#scale[1.3]{Preliminary}')
txt.Draw('same')
#prim.InsertText('Primary Mirror')
prim.Draw('same')
date.Draw('same')
leg.Draw('same')
middle.Draw('same')
#pri_upper.Draw('same')
#pri_lower.Draw('same')
#flip1.Draw('same')
#flip2.Draw('same')
#flip3.Draw('same')
#flip4.Draw('same')

cPlot.SaveAs("plots/Rich1_allin1_20170830.pdf")
multisPriZ.Draw('AP')
multisPriZ.GetXaxis().SetTitle("Alignment number [a.u.]")
multisPriZ.GetYaxis().SetTitle("#DeltaZ Variation [mrad]")
flip1pZ_17.Draw('same')
flip2pZ_17.Draw('same')
pZ_17_line1.Draw('same')
pZ_17_line2.Draw('same')
txt.Draw('same')
prim.Draw('same')
date.Draw('same')
leg.Draw('same')
#pri_upper.Draw('same')
#pri_lower.Draw('same')
#flip1.Draw('same')
#flip2.Draw('same')
#flip3.Draw('same')
#flip4.Draw('same')

middle.Draw('same')
cPlot.SaveAs("plots/Rich1_allin1_20170830.pdf")
multisSecY.Draw('AP')
multisSecY.GetXaxis().SetTitle("Alignment number [a.u.]")
multisSecY.GetYaxis().SetTitle("#DeltaY Variation [mrad]")
flip1sY_17.Draw('same')
flip2sY_17.Draw('same')
sY_17_line1.Draw('same')
sY_17_line2.Draw('same')
txt.Draw('same')
leg2a.InsertText('the 16 individual mirrors.')
leg2a.Draw('same')
#leg2b.Draw('same')
date.Draw('same')
middle.Draw('same')
#sec_upper.Draw('same')
#sec_lower.Draw('same')
#flip1s.Draw('same')
#flip2s.Draw('same')
#flip3s.Draw('same')
#flip4s.Draw('same')
seco.Draw('same')

cPlot.SaveAs("plots/Rich1_allin1_20170830.pdf")
multisSecZ.Draw('AP')
multisSecZ.GetXaxis().SetTitle("Alignment number [a.u.]")
multisSecZ.GetYaxis().SetTitle("#DeltaZ Variation [mrad]")
flip1sZ_17.Draw('same')
flip2sZ_17.Draw('same')
sZ_17_line1.Draw('same')
sZ_17_line2.Draw('same')
txt.Draw('same')
leg2a.Draw('same')
#leg2b.Draw('same')
date.Draw('same')
middle.Draw('same')
#sec_upper.Draw('same')
#sec_lower.Draw('same')
#flip1s.Draw('same')
#flip2s.Draw('same')
#flip3s.Draw('same')
#flip4s.Draw('same')
seco.Draw('same')

cPlot.SaveAs("plots/Rich1_allin1_20170830.pdf")

cPlot.SaveAs("plots/Rich1_allin1_20170830.pdf]")

#  LocalWords:  pri
